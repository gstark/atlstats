# ATLAS Statistics Software Stack

Core software to the ATLAS Statistics Software Stack

[![pipeline status](https://gitlab.cern.ch/atlas-amglab/atlstats/badges/master/pipeline.svg)](https://gitlab.cern.ch/atlas-amglab/atlstats/commits/master)

## Docker Images

Docker images with ROOT 6 and Python 3 that containerize the ATLAS Statistics Software Stack

- [`root-base`][root-base-docker-hub]: ROOT without stats packages
- [`stats-base`][stats-base-docker-hub]: ROOT with stats packages (RooFit, RooFitCore, RooStats, HistFactory, MINUIT)
- [`stats`](https://hub.docker.com/r/atlasamglab/stats): `stats-base` with [`RooFitExtensions`](https://gitlab.cern.ch/atlas_higgs_combination/software/RooFitExtensions)


[root-base-docker-hub]: https://hub.docker.com/r/atlasamglab/root-base
[stats-base-docker-hub]: https://hub.docker.com/r/atlasamglab/stats-base
